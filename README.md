# Games You Own BFF
This project is the backend for frontend for Games You Own app.
It was developed with Spring boot, lombok and mongodb to persist user data.
It provides a endpoint with game data from [Rawg.io API](https://rawg.io/apidocs).
  
## Running in dev environment

Run `./gradlew build` and `./gradlew bootRun`. The app will start in `http://localhost:5000/`

You also can run in local using docker-compose:
1. In `application-prod.properties` set the `config.disableCors` to true
2. Run `./gradlew build`
3. Run `docker-compose up -d`

Make sure you don't have any app running in port 5000.


## About the assignment

- Use of Spring boot suit and Java 11
- The app is automatically deployed when a merge is made on `master` branch, using jenkins and gitlab integration via webhook
- In prod, I deployed the apps in my personal cloud and the endpoints are routed internally for each docker container. 
The route is made by a nginx server for a specific https subdomain in the following way:
: `https://games-you-won.jsouza.dev/*` for frontend app and `https://games-you-won.jsouza.dev/api/*` for backend requests.

