package com.translucent.gamesyouown.domain.dto.rawgapi;

import lombok.Data;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Data
public class Platform {
	private String name;
}
