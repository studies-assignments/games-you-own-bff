package com.translucent.gamesyouown.domain.dto;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
public enum CompletitionStatus {
	NOT_COMPLETED, IN_PROGRESS, COMPLETED
}
