package com.translucent.gamesyouown.domain.dto.rawgapi;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Data
public class ApiResponse {
	private List<GameApiResponse> results = new ArrayList<>();
}
