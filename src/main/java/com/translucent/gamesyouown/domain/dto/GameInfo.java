package com.translucent.gamesyouown.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Data
public class GameInfo {

	private Integer apiId;
	private String name;
	private Integer releasedYear;
	private List<Console> console;
	private String imageUrl;

}
