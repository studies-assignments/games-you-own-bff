package com.translucent.gamesyouown.domain.dto.rawgapi;

import lombok.Data;

import java.util.List;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Data
public class GameApiResponse {

	private String id;
	private String name;
	private String released;
	private String background_image;
	private List<PlatformInfo> platforms;

}
