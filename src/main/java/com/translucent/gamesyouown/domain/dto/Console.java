package com.translucent.gamesyouown.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@AllArgsConstructor
@Getter
public enum Console {
	PS4("18"), XBOX_ONE("1"), NINTENDO_SWITCH("7"), PS5("187"), PC("4");

	private final String apiPlatformId;

}


