package com.translucent.gamesyouown.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.translucent.gamesyouown.domain.dto.CompletitionStatus;
import com.translucent.gamesyouown.domain.dto.GameInfo;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Data
public class GameEntity {

	@Id
	private String id;
	private GameInfo info;
	private LocalDate completitionDate;
	private CompletitionStatus completitionStatus;
	private String personalNotes;





}
