package com.translucent.gamesyouown.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Configuration
public class SpringConfiguration implements WebMvcConfigurer {

	@Value("${config.disableCors}")
	private Boolean disableCors;

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		if (disableCors) {
			registry.addMapping("/**").allowedMethods("*");
		}
	}
}
