package com.translucent.gamesyouown.service;

import com.translucent.gamesyouown.domain.dto.Console;
import com.translucent.gamesyouown.domain.GameEntity;
import com.translucent.gamesyouown.domain.dto.rawgapi.ApiResponse;
import com.translucent.gamesyouown.repository.GameRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GameService {

	private final GameRepository repository;
	private final RawgGamesApiService gamesApiService;

	public ApiResponse searchAllGames(String name) {
		return gamesApiService.searchGamesInApi(name);
	}

	public List<GameEntity> searchInOwnGames(String name, Console console) {
		return this.repository.search(name, console);
	}

	public GameEntity save(GameEntity game) {
		if (Strings.isNotEmpty(game.getId())) {
			throwsBadRequestIfGameNotExists(game.getId());
		}

		return this.repository.save(game);
	}

	public Optional<GameEntity> findById(String id) {
		return this.repository.findById(id);
	}


	public void delete(String id) {
		throwsBadRequestIfGameNotExists(id);

		this.repository.deleteById(id);
	}

	private void throwsBadRequestIfGameNotExists(String id) {
		var optGame = this.repository.findById(id);

		if (optGame.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Game ID");
		}
	}

}
