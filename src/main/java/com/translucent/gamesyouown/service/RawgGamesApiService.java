package com.translucent.gamesyouown.service;

import com.translucent.gamesyouown.domain.dto.Console;
import com.translucent.gamesyouown.domain.dto.rawgapi.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RawgGamesApiService {

	@Value("${rawg.apiurl}")
	private String apiUrl;

	@Value("${rawg.apikey}")
	private String apiKey;

	@Value("${rawg.pageSize}")
	private String pageSize;


	public ApiResponse searchGamesInApi(String name) {
		var rest = new RestTemplate();

		var platforms = Arrays.stream(Console.values())
				.map(Console::getApiPlatformId)
				.collect(Collectors.joining(","));

		var url = this.apiUrl + "/games?key={key}&page_size={pageSize}&search={name}&platforms={platforms}";

		try {
			var response = rest.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, ApiResponse.class,
					this.apiKey, this.pageSize, name, platforms);
			return response.getBody();
		} catch (Exception e) {
			log.error("Failed when trying to request the game list", e);
		}
		return new ApiResponse();
	}

}
