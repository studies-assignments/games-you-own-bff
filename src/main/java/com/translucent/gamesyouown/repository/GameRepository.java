package com.translucent.gamesyouown.repository;

import com.translucent.gamesyouown.domain.GameEntity;
import com.translucent.gamesyouown.repository.custom.GameCustomRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Repository
public interface GameRepository extends MongoRepository<GameEntity, String>, GameCustomRepository {

}
