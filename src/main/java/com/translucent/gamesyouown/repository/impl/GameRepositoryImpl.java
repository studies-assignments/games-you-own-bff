package com.translucent.gamesyouown.repository.impl;

import com.translucent.gamesyouown.domain.dto.Console;
import com.translucent.gamesyouown.domain.GameEntity;
import com.translucent.gamesyouown.repository.custom.GameCustomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GameRepositoryImpl implements GameCustomRepository {

	private final MongoTemplate mongoTemplate;

	@Override
	public List<GameEntity> search(String name, Console console) {
		var criteria = Criteria.where("info.name").regex(name, "i");

		if (Objects.nonNull(console)) {
			criteria = criteria.andOperator(Criteria.where("info.console").is(console));
		}

		var query = Query.query(criteria)
				.with(Sort.by("info.releasedYear").descending());

		return this.mongoTemplate.find(query, GameEntity.class);
	}
}
