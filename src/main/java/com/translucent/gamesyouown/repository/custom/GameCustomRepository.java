package com.translucent.gamesyouown.repository.custom;

import com.translucent.gamesyouown.domain.dto.Console;
import com.translucent.gamesyouown.domain.GameEntity;

import java.util.List;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
public interface GameCustomRepository {
	List<GameEntity> search(String name, Console console);
}
