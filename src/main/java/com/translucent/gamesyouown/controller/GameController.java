package com.translucent.gamesyouown.controller;

import com.translucent.gamesyouown.domain.dto.Console;
import com.translucent.gamesyouown.domain.GameEntity;
import com.translucent.gamesyouown.domain.dto.rawgapi.GameApiResponse;
import com.translucent.gamesyouown.service.GameService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author José Victor | jvas.2000@gmail.com
 */
@RestController
@RequestMapping("/games")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GameController {

	private final GameService service;

	@GetMapping("/all")
	public List<GameApiResponse> searchAllGames(@RequestParam() String name) {
		return this.service.searchAllGames(name).getResults();
	}

	@GetMapping("/{id}")
	public GameEntity searchInOwnGames(@PathVariable("id") String id) {
		return this.service.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Game ID"));
	}

	@GetMapping
	public List<GameEntity> searchInOwnGames(@RequestParam(defaultValue = "") String name, @RequestParam(required = false) Console console) {
		return this.service.searchInOwnGames(name, console);
	}

	@PostMapping
	public GameEntity save(@RequestBody GameEntity game) {
		return this.service.save(game);
	}

	@PutMapping
	public GameEntity update(@RequestBody GameEntity game) {
		if (Strings.isEmpty(game.getId())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Game ID");
		}

		return this.service.save(game);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") String id) {
		this.service.delete(id);
		return ResponseEntity.ok().build();
	}

}
