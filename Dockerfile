FROM openjdk:13-jdk-alpine

COPY build/libs/*.jar app.jar

EXPOSE 5000
ENTRYPOINT ["java", "-jar", "/app.jar" ]
